/**
 * @file   pic18fxx2_isibot_servo_protocol.h
 * @author ISIBOT Francois LEBORNE
 * @date 2013-12-19
 * @brief surcouche du protocole I2C pour commander les servos
 */

#ifndef PIC18FXX2_ISIBOT_SERVO_PROTOCOL_H
#define PIC18FXX2_ISIBOT_SERVO_PROTOCOL_H

#define FIFO_SIZE 64
#define END_OF_TRAME 0xFF
#define TRAME_SIZE 9

/**
 * @enum cmdId
 * @brief enum pour les identifiants des commandes des servos
 */
typedef enum cmdId
{
	ENABLE,
	DISABLE,
	SET_ANGLE,
	ROTATE,
	ENABLE_SET_ANGLE,
	CONFIG
} cmdId;

/**
 * @struct ServoCommand
 * @brief regroupement de tous les parametres possibles pour une commande de servo
 */
typedef struct ServoCommand
{
     char numCmd;
     char numServo;
     int angle1;
     int angle2;
     int offset1;
     int offset2;
} ServoCommand;

/* fonctions de gestion des commandes */
char parseCmd(char trame[TRAME_SIZE], ServoCommand *cmd);
char execCmd(ServoCommand cmd);

/* fonctions principales de gestion des donnees */
char bufferManager(FIFO*, char trame[TRAME_SIZE]);
void I2C_ServoController(void);

#endif