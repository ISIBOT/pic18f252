/**
 * @file pic18f252_isibot_servos.h
 * @author ISIBOT Lelong G�rald
 * @date 2013-12-19
 */

#ifndef PIC18F252_ISIBOT_SERVOS_H
#define	PIC18F252_ISIBOT_SERVOS_H

#define bool char
#define true 1
#define false 0

typedef enum ServoPin
{
    PIN0 = 0,
    PIN1,
    PIN2,
    PIN3,
    PIN4,
    PIN5,
    PIN6,
    PIN7
} ServoPin;

typedef enum ServoState
{
    DISABLED,
    ENABLED
} ServoState;

typedef struct ServoConf
{
    int min_angle;
    int max_angle;
    int min_pulse;
    int max_pulse;
} ServoConf;

typedef struct Servo_t
{
    int angle;
    unsigned char offset;
    struct Servo_t* next;
    unsigned char pin;
    ServoState state;
    ServoConf conf;
} Servo;

void initServos(void);

bool setServoConf(ServoPin n_pin, ServoConf n_conf);

void setServo(ServoPin n_pin, ServoState n_state);

bool setAngle(ServoPin n_pin, int n_angle);

bool rotate(ServoPin n_pin, int n_rotation);

unsigned char computeOffset(int n_angle, ServoConf n_conf);

void newPeriod(void);

void updateNextServoOnTimerIT(void);

void nextEnabledServo(void);

#endif	/* PIC18F252_ISIBOT_SERVOS_H */

