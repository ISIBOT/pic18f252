/**
 * @file   pic18fxx2_isibot_buffer.h
 * @author ISIBOT Francois LEBORNE
 * @date 2014-01-23
 * @brief gestion d'un buffer
 */

#ifndef PIC18FXX2_ISIBOT_BUFFER_H
#define PIC18FXX2_ISIBOT_BUFFER_H

#define FIFO_SIZE 64

/**
 * @struct FIFO
 * @brief representation du buffer
 */
typedef struct FIFO {
	char data[FIFO_SIZE];
	char start;
	char end;
	char sloubi; // nb d'elements
} FIFO;

void initBuffer(FIFO*);
char putcBuffer(FIFO*, char);
char getcBuffer(FIFO*);
char emptyBuffer(FIFO*); // = is empty ?
void cleanBuffer(FIFO*);

#endif