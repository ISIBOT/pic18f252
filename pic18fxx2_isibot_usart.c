/**
 * @file pic18fxx2_isibot_usart.c
 * @author ISIBOT Francois Leborne
 * @date 2014-01-15
 */

#include <p18f252.h>
#include <usart.h>

void lowIT(void);
void highIT(void);

char onCharReceptionDefault(char c)
{
    return 0x00;
}

void initUSART(void)
{
    //onUSART_CharReception = n_onUSART_CharReception;

    // set RC6/TX as an output and RC7/RX as an input
    /*TRISCbits.RC6 = 0;
    TRISCbits.RC7 = 1;*/

    // enable priority levels on interrupts
    RCONbits.IPEN = 1;

    // enables all low priority interrupts
    INTCONbits.GIE = 1;
    INTCONbits.GIEL = 1;
}

/*#pragma code low_it = 0x18 //0x18 pour basse priorit�
void onLowIT(void)
{
    _asm GOTO lowIT _endasm
}
#pragma code

#pragma interrupt lowIT
void lowIT(void)
{
    char data;
    data = 0x00;

    putcUSART('i');

    if(PIR1bits.RCIF)
    {
        data = getcUSART();
        putcUSART(data);
        PIR1bits.RCIF = 0;
        //(*onUSART_CharReception)(data);
    }
    else
    {
        putcUSART('0');
    }
}

#pragma code high_it = 0x08
void onHighIT(void)
{
    _asm GOTO highIT _endasm
}
#pragma code

#pragma interrupt highIT
void highIT(void)
{
    putcUSART('i');

    char data;
    data = 0x00;

    if(PIR1bits.RCIF)
    {
        data = getcUSART();
        putcUSART(data);
        PIR1bits.RCIF = 0;
        //(*onUSART_CharReception)(data);
    }
    else
    {
        putcUSART('0');
    }
}*/