/**
 * @file   pic18fxx2_isibot_servo_protocol.c
 * @author ISIBOT Francois LEBORNE
 * @date 2013-12-19
 * @brief surcouche du protocole I2C pour commander les servos
 */

#include "pic18fxx2_isibot_buffer.h"
#include "pic18fxx2_isibot_i2c.h"
#include "pic18f252_isibot_servos.h"
#include "pic18fxx2_isibot_servo_protocol.h"

extern FIFO buffer_reception;
extern FIFO buffer_emission;

/**
 * @brief nettoyage du buffer
 */
void cleanI2C_Buffer(FIFO * buffer)
{
    while ((!emptyBuffer(buffer)) && (I2C_Rcv() != END_OF_TRAME));
}

/**
 * @brief gestion du buffer
 * @return une trame bien formatee pour commander les servos
 */
char bufferManager(FIFO * buffer, char trame[TRAME_SIZE])
{
    char numCmd, i, err;
    err = 0;
    trame[0] = I2C_Rcv();
    numCmd = (char)((trame[0] >> 4) & 0x07);

    /* suivant le type de commande, on lit un certain nombre de caracteres
       dans le buffer */
    switch(numCmd)
    {
        case 2:
        case 3:
        case 4:
            trame[1] = I2C_Rcv();
            trame[2] = I2C_Rcv();
            break;
        case 5:
            for (i=1; i<TRAME_SIZE; ++i)
                trame[i] = I2C_Rcv();
            break;
        default:
            err = 1;
            break;
    }
    // on lit tout ce qui peut rester dans le buffer jusqu'au caractere de fin de trame
    cleanI2C_Buffer(buffer);

    return err;
}

/**
 * @brief parse une trame de commande des servos pour remplir les variables de commande
 * @param trame: la trame a parser
 * @param cmd: struct stockant les parametres de la commande
 * @return code d'erreur a 1 si le num du servo est incoherent
 */
char parseCmd(char trame[TRAME_SIZE], ServoCommand *cmd)
{
    char err;
    err = 0;

    cmd->numCmd = (trame[0] >> 4) & 0x07;
    cmd->numServo = trame[0] & 0x0F;

    if(cmd->numServo < 0 || cmd->numServo > 7)
    {
        err = 1;
    }
    else
    {
        //angle1 lu sur trame[1] et trame[2]
        cmd->angle1 = (int) trame[1];
        cmd->angle1 = (cmd->angle1 << 8) & 0xFF00;
        cmd->angle1 += ((int) trame[2]) & 0x00FF;

        //angle2 lu sur trame[3] et trame[4]
        cmd->angle2 = (int) trame[3];
        cmd->angle2 = (cmd->angle2 << 8) & 0xFF00;
        cmd->angle2 += ((int) trame[4]) & 0x00FF;

        cmd->offset1 = (int) trame[5];
        cmd->offset1 = (cmd->offset1 << 8) & 0xFF00;
        cmd->offset1 += ((int) trame[6]) & 0x00FF;

        cmd->offset2 = (int) trame[7];
        cmd->offset2 = (cmd->offset2 << 8) & 0xFF00;
        cmd->offset2 += ((int) trame[8]) & 0x00FF;
    }
    
    return err;
}

/**
 * @brief execute une commande
 * @param cmd: commande a executer
 * @return
 */
char execCmd(ServoCommand cmd)
{
    int err;
    ServoConf conf;
    err = 0;
    
    switch (cmd.numCmd)
    {
        case ENABLE:
            setServo((ServoPin) cmd.numServo, ENABLED);
            break;
        case DISABLE:
            setServo((ServoPin) cmd.numServo, DISABLED);
            break;
        case SET_ANGLE:
            setAngle((ServoPin) cmd.numServo, cmd.angle1);
            break;
        case ROTATE:
            rotate((ServoPin) cmd.numServo, cmd.angle1);
            break;
        case ENABLE_SET_ANGLE:
            setServo((ServoPin) cmd.numServo, ENABLED);
            setAngle((ServoPin) cmd.numServo, cmd.angle1);
            break;
        case CONFIG:
            conf.min_angle = cmd.angle1;
            conf.max_angle = cmd.angle2;
            conf.min_pulse = cmd.offset1;
            conf.max_pulse = cmd.offset2;

            setServoConf((ServoPin) cmd.numServo, conf);
            break;
        default:
            err = 1;
    }
    return err;
}

/**
 * @brief programme autonome pour gerer les servos depuis des commandes reçues par I2C
 */
void I2C_ServoController()
{
    ServoCommand cmd;
    char trame[TRAME_SIZE] = {0};
    char err = 0;

    cmd.angle1 = 0;
    cmd.angle2 = 0;
    cmd.numCmd = 0;
    cmd.numServo = 0;
    cmd.offset1 = 0;
    cmd.offset2 = 0;

    I2C_Init(0xC0);
    initServos();

    while (1)
    {
        while(emptyBuffer(&buffer_reception));

        err = bufferManager(&buffer_reception, trame);
        err |= parseCmd(trame, &cmd);
        err |= execCmd(cmd);

        cleanBuffer(&buffer_emission);
        I2C_Send(err);
    }
}
