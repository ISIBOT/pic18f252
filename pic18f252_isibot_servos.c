/**
 * @file pic18f252_isibot_servos.c
 * @author ISIBOT Lelong G�rald
 * @date 2013-12-19
 */

#include <p18f252.h>
#include "pic18f252_isibot_servos.h"
#include <timers.h>

#define PORTB_MASK 0b00111111
#define PORTC_MASK 0b11000000
#define NB_SERVOS 8;

Servo * managedServo;
Servo m_servos[8]; // size must match NB_SERVOS
ServoConf defaultConf;

void initServos(void)
{
    unsigned char i;

    managedServo = 0;

    // Port configuration
    TRISB =  TRISB & ~PORTB_MASK;
    //TRISC = TRISC & ~PORTC_MASK;
    LATB =  LATB & ~PORTB_MASK;
    //LATC = LATC & ~PORTC_MASK;

    // TIMER0 configuration
    T0CONbits.TMR0ON = 0;
    T0CONbits.T08BIT = 0;
    T0CONbits.T0PS = 0b000;
    T0CONbits.PSA = 0;
    T0CONbits.T0CS = 0;
    INTCONbits.TMR0IF = 0;
    INTCON2bits.TMR0IP = 1;
    INTCONbits.TMR0IE = 1;

    // TIMER2 configuration
    OpenTimer2(TIMER_INT_ON &
               T2_PS_1_16 &
               T2_POST_1_4);
    T2CONbits.TMR2ON = 0;
    PIR1bits.TMR2IF = 0;
    IPR1bits.TMR2IP = 1;
    PIE1bits.TMR2IE = 1;

    defaultConf.min_angle = 0;
    defaultConf.max_angle = 180;
    defaultConf.min_pulse = 400;
    defaultConf.max_pulse = 2300;

    for(i=0; i<7; ++i) // index max must match NB_SERVOS-1
    {
        m_servos[i].conf = defaultConf;
        m_servos[i].angle = defaultConf.min_angle;
        m_servos[i].offset = defaultConf.min_pulse;
        m_servos[i].next = &(m_servos[i+1]);
        m_servos[i].pin = 0x01 << i;
        m_servos[i].state = DISABLED;
    }

    m_servos[i].conf = defaultConf;
    m_servos[i].angle = defaultConf.min_angle;
    m_servos[i].offset = defaultConf.min_pulse;
    m_servos[i].next = 0;
    m_servos[i].pin = 0x01 << i;
    m_servos[i].state = DISABLED;

    managedServo = 0;

    // Interrupt configuration
    RCONbits.IPEN = 1;
    INTCONbits.GIEH = 1;

    T0CONbits.TMR0ON = 1;
}

bool setServoConf(ServoPin n_pin, ServoConf n_conf)
{
    if(n_pin >=0 && n_pin <= 7 &&
       n_conf.max_angle > n_conf.min_angle &&
       n_conf.max_pulse > n_conf.min_pulse)
    {
        m_servos[n_pin].conf = n_conf;
        return true;
    }

    return false;
}

void setServo(ServoPin n_pin, ServoState n_state)
{   
    if(n_pin >=0 && n_pin <= 7)
    {
        m_servos[n_pin].state = n_state;
    }
}

bool setAngle(ServoPin n_pin, int n_angle)
{
    switch(n_pin)
    {
        case PIN0:
            break;
        case PIN1:
            break;
        case PIN2:
            break;
        case PIN3:
            break;
        case PIN4:
            break;
        case PIN5:
            break;
        case PIN6:
            break;
        case PIN7:
            break;
        default:
            break;
    }
    if(n_pin >=0 && n_pin <= 7 &&
       n_angle >= m_servos[n_pin].conf.min_angle &&
       n_angle <= m_servos[n_pin].conf.max_angle)
    {
        m_servos[n_pin].angle = n_angle;
        m_servos[n_pin].offset = computeOffset(n_angle, m_servos[n_pin].conf);

        return true;
    }

    return false;
}

bool rotate(ServoPin n_pin, int n_rotation)
{
    if(n_pin >=0 && n_pin <= 7)
    {
        return setAngle(n_pin, m_servos[n_pin].angle + n_rotation);
    }
    else return false;
}
unsigned char computeOffset(int n_angle, ServoConf conf)
{
    int t_pulse;
    unsigned char t_offset;
    /*t_pulse = (int)(((float)(conf.max_pulse - conf.min_pulse) *
           (float)(n_angle - conf.min_angle) / (float)(conf.max_angle - conf.min_angle)
           + (float)conf.min_pulse));*/
    t_pulse = (int)((conf.max_pulse - conf.min_pulse) *
           ((n_angle - conf.min_angle) / (float)(conf.max_angle - conf.min_angle))
           + conf.min_pulse);
    t_offset = (unsigned char)(t_pulse * 10 / 128); // Tick = 12.8�s
    return t_offset;
}

void newPeriod(void)
{
    managedServo = &m_servos[0];
    if(managedServo->state != ENABLED) nextEnabledServo();

    if(managedServo)
    {
        PIR1bits.TMR2IF = 0;
        T2CONbits.TMR2ON = 1;
        PR2 = managedServo->offset;
        TMR2 = 0x00;
        LATB = managedServo->pin & PORTB_MASK;
        //LATC = managedServo->pin & PORTC_MASK;
    }

    TMR0H = 0x3D;
    TMR0L = 0x00;
}

void updateNextServoOnTimerIT(void)
{
    bool are_all_interrupts_handled = false;

    while(!are_all_interrupts_handled)
    {
        if(PIR1bits.TMR2IF)
        {
            if(managedServo)
            {
                LATB &= ~(managedServo->pin & PORTB_MASK);
                //LATC &= ~(managedServo->pin & PORTC_MASK);
                nextEnabledServo();

                if(managedServo)
                {
                    PR2 = managedServo->offset;
                    //WriteUSART(managedServo->offset);
                    TMR2 = 0x00;
                    LATB |= managedServo->pin & PORTB_MASK;
                    //LATC |= managedServo->pin & PORTC_MASK;
                }
                else T2CONbits.TMR2ON = 0;
            }

            PIR1bits.TMR2IF = 0;
        }
        else if(INTCONbits.TMR0IF)
        {
            newPeriod();
            INTCONbits.TMR0IF = 0;
        }
        else are_all_interrupts_handled = true;
    }
}

void nextEnabledServo(void)
{
    /*while(managedServo && (managedServo->state != ENABLED))
    {
        managedServo = managedServo->next;
    }*/
    bool is_servo_enabled = false;

    while(managedServo && !is_servo_enabled)
    {
        managedServo = managedServo->next;

        if(managedServo)
        {
            is_servo_enabled = managedServo->state;
        }
    }
}

#pragma interrupt highIT
void highIT(void)
{
    updateNextServoOnTimerIT();
}

#pragma code high_vector=0x08 // Haute priorit�
void onHighIT(void)
{
    _asm GOTO highIT _endasm
}
