/**
 * @file pic18fxx2_isibot_i2c.c
 * @author ISIBOT Francois Leborne
 * @date 2013-12-19
 */

#include "pic18fxx2_isibot_buffer.h"
#include "pic18fxx2_isibot_i2c.h"
#include <p18f252.h>
#include <i2c.h>

FIFO buffer_reception;
FIFO buffer_emission;

void lowIT(void);

/**
 * @brief initialise le bus i2c materiellement et logiciellement
 */
void I2C_Init(char addr)
{
    TRISC = TRISC | 0x18;
    OpenI2C(SLAVE_7, SLEW_OFF);
    SSPADD = addr;
    SSPCON2bits.SEN = 1;
    SSPCON1bits.CKP = 1;
    IPR1bits.SSPIP = 0;
    PIR1bits.SSPIF = 0;
    PIE1bits.SSPIE = 1;
    RCONbits.IPEN = 1;
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;
    
    initBuffer(&buffer_reception);
    initBuffer(&buffer_emission);
}

/**
 * @brief est appelee quand le PIC recoit une demande d'emission mais qu'il n'y
 *  a rien dans le buffer. Mettre le code adequat dans le corps de cette fonction
 */
void fillBuffer(void)
{
    char c;
    //volatile int i;
    //for (i=0; i<0xFFFF; i++);
    //for (i=0; i<0xFFFF; i++);
    //putcBuffer(&buffer_emission, 0xFF);


    c = I2C_Rcv();
    I2C_flush();
    I2C_Send(c);
}

/**
 * @brief vide le buffer d'emission
 */
void I2C_flush(void)
{
    cleanBuffer(&buffer_emission);
}

/** PAS TRES UTILE car rcv attend deja que la donnee soit prete (buffer)
 * @brief indique s'il y a des donnees a lire sur le bus
 * @return 0 s'il n'y a rien a lire, 1 sinon
 */
char I2C_DataRdy(void)
{
    return (!emptyBuffer(&buffer_reception));
}

/**
 * @brief envoie un caractere sur le bus i2c
 * @param c le caractere a envoyer
 */
void I2C_Send(char c)
{
    putcBuffer(&buffer_emission, c);
}

/**
 * @brief lit un caractere sur le bus i2c
 * @return le caractere lu
 */
char I2C_Rcv(void)
{
    return getcBuffer(&buffer_reception);
}

#pragma code low_it = 0x18 //0x18 pour basse priorite
void onLowIT(void)
{
    _asm GOTO lowIT _endasm
}
#pragma code

#pragma interrupt lowIT
void lowIT(void)
{
    char addr, data;
    data = 0x00;
    
    if(PIR1bits.SSPIF == 1 && SSPSTATbits.BF == 1 && SSPSTATbits.D_A == 0)
    {
        addr = SSPBUF;
        PIR1bits.SSPIF = 0;
        SSPCON1bits.CKP = 1;
    }

    if(!SSPSTATbits.R_W && PIR1bits.SSPIF == 1 && SSPSTATbits.BF == 1 && SSPSTATbits.D_A == 1)
    {
        data = SSPBUF;
        PIR1bits.SSPIF = 0;
        putcBuffer(&buffer_reception, data);

        if(SSPCON1bits.SSPOV)
        {
            SSPCON1bits.SSPOV = 0;
        }

        SSPCON1bits.CKP = 1;
    }

    if(SSPSTATbits.R_W && PIR1bits.SSPIF == 1)
    {
        // clear reception/transmission flag
        PIR1bits.SSPIF = 0;
        
        // load SSPBUF with a data
        if (emptyBuffer(&buffer_emission))
            fillBuffer();

        SSPBUF = getcBuffer(&buffer_emission);
        
        // release clock
        SSPCON1bits.CKP = 1;
    }
}
