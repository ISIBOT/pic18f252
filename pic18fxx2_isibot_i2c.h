/**
 * @file pic18fxx2_isibot_i2c.h
 * @author ISIBOT Francois Leborne
 * @date 2013-12-19
 */

#ifndef PIC18FXX2_ISIBOT_I2C_H
#define	PIC18FXX2_ISIBOT_I2C_H

/**
 * @brief vide le buffer d'emission
 */
void I2C_flush(void);

/**
 * @brief est appelee quand le PIC recoit une demande d'emission mais qu'il n'y
 *  a rien dans le buffer. Mettre le code adequat dans le corps de cette fonction
 */
void fillBuffer(void);

/**
 * @brief initialise le bus i2c materiellement et logiciellement
 */
void I2C_Init(char addr);

/**
 * @brief envoie un caractere sur le bus i2c
 * @param c le caractere a envoyer
 */
void I2C_Send(char);

/**
 * @brief indique s'il y a des donnees a lire sur le bus
 * @return 0 s'il n'y a rien a lire, 1 sinon
 */
char I2C_DataRdy(void);

/**
 * @brief lit un caractere sur le bus i2c
 * @return le caractere lu
 */
char I2C_Rcv(void);

#endif	/* PIC18FXX2_ISIBOT_I2C_H */

