/**
 * @file   pic18fxx2_isibot_buffer.c
 * @author ISIBOT Francois LEBORNE
 * @date 2014-01-23
 * @brief gestion d'un buffer
 */

#include "pic18fxx2_isibot_buffer.h"

void initBuffer(FIFO * buffer)
{
   buffer->sloubi = 0;
   buffer->start = 0;
   buffer->end = -1;
}

/**
 * @brief ecriture
 * @param le caractere a ecrire dans le buffer
 * @return code d'erreur (0 en cas d'erreur)
 */
char putcBuffer(FIFO * buffer, char c)
{
    char success;
    success = 1;

    if (buffer->sloubi < FIFO_SIZE)
    {
       buffer->end = (buffer->end + 1) % FIFO_SIZE;
       buffer->sloubi = buffer->sloubi + 1;
       buffer->data[buffer->end] = c;
    }
    else
    {
        success = 0;
    }

    return success;
}

/**
 * @brief lecture d'un caractere du buffer. Bloquante si buffer vide
 * @return caractere lu dans le buffer
 */
char getcBuffer(FIFO * buffer)
{
    char c;
    c = 0x00;

    while(emptyBuffer(buffer));

    buffer->start = (buffer->start + 1) % FIFO_SIZE;
    buffer->sloubi = buffer->sloubi - 1;
    c = buffer->data[(buffer->start - 1) % FIFO_SIZE];

    return c;
}

/**
 * @brief le buffer est-il vide ?
 * @return 1 si le buffer est vide, 0 sinon
 */
char emptyBuffer(FIFO * buffer)
{
    return (buffer->sloubi == 0);
}

/**
 * @brief vidage du buffer
 */
void cleanBuffer(FIFO * buffer)
{
   buffer->sloubi = 0;
   buffer->start = 0;
   buffer->end = -1;
}